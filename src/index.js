/* retrns null if set intersection contains less than threshold members*/
 

Set.prototype.minimumIntersection = function(other, threshold) {
	var intersection = new Set();
	var reached = 0


	let a, b
	if (this.size > other.size) {
		a = this
		b = other
	} else {
		b = this
		a = other
	}

	var leftToCheck = b.size
	for (var elem of b) {
		if ((leftToCheck+reached) < threshold) {
			return null
		}
		if (a.has(elem)) {
			intersection.add(elem);
			reached ++
		}
		leftToCheck --
	}
	if (reached < threshold) {
		return null
	}
	return  intersection
}


const SPLITTER_REGEX = /[\s,\.]+/


const STOPWORDS = {
	// small: [
	// "I", "A", "ABOUT", "AN", "ARE", "AS", "AT", "BE", "BY", "COM", 
	// "FOR", "FROM","HOW","IN", "IS", "IT", "OF", "ON", "OR", "THAT",
	// "THE", "THIS","TO", "WAS", "WHAT", "WHEN", "WHERE", "WHO", "WILL", 
	// "WITH", "THE"
	// ],
	large: [
	"A", "ABOUT", "ABOVE", "AFTER", "AGAIN", "AGAINST", "ALL", "AM", "AN", "AND", "ANY", "ARE", "AREN'T", 
	"AS", "AT", "BE", "BECAUSE", "BEEN", "BEFORE", "BEING", "BELOW", "BETWEEN", "BOTH", "BUT", "BY", "CAN'T", 
	"CANNOT", "COULD", "COULDN'T", "DID", "DIDN'T", "DO", "DOES", "DOESN'T", "DOING", "DON'T", "DOWN", 
	"DURING", "EACH", "FEW", "FOR", "FROM", "FURTHER", "HAD", "HADN'T", "HAS", "HASN'T", "HAVE", "HAVEN'T", 
	"HAVING", "HE", "HE'D", "HE'LL", "HE'S", "HER", "HERE", "HERE'S", "HERS", "HERSELF", "HIM", "HIMSELF", 
	"HIS", "HOW", "HOW'S", "I", "I'D", "I'LL", "I'M", "I'VE", "IF", "IN", "INTO", "IS", "ISN'T", "IT", "IT'S", 
	"ITS", "ITSELF", "LET'S", "ME", "MORE", "MOST", "MUSTN'T", "MY", "MYSELF", "NO", "NOR", "NOT", "OF", 
	"OFF", "ON", "ONCE", "ONLY", "OR", "OTHER", "OUGHT", "OUR", "OURS	", "OURSELVES", "OUT", "OVER", "OWN", 
	"SAME", "SHAN'T", "SHE", "SHE'D", "SHE'LL", "SHE'S", "SHOULD", "SHOULDN'T", "SO", "SOME", "SUCH", "THAN", 
	"THAT", "THAT'S", "THE", "THEIR", "THEIRS", "THEM", "THEMSELVES", "THEN", "THERE", "THERE'S", "THESE", 
	"THEY", "THEY'D", "THEY'LL", "THEY'RE", "THEY'VE", "THIS", "THOSE", "THROUGH", "TO", "TOO", "UNDER", 
	"UNTIL", "UP", "VERY", "WAS", "WASN'T", "WE", "WE'D", "WE'LL", "WE'RE", "WE'VE", "WERE", "WEREN'T", 
	"WHAT", "WHAT'S", "WHEN", "WHEN'S", "WHERE", "WHERE'S", "WHICH", "WHILE", "WHO", "WHO'S", "WHOM", "WHY", 
	"WHY'S", "WITH", "WON'T", "WOULD", "WOULDN'T", "YOU", "YOU'D", "YOU'LL", "YOU'RE", "YOU'VE", "YOUR", 
	"YOURS", "YOURSELF", "YOURSELVES"
	]
}


exports.importantWords = (phrase) => {
	if (!phrase) return []
		return phrase.toUpperCase().split(SPLITTER_REGEX).filter((word) => {
			return (word && (!STOPWORDS.large.some((stopword) => stopword===word)))
		})
}


exports.phrasesMatch = (words1, words2, threshold) => {
	if (! (words1 && words2)) {
		return false
	}
	if (threshold < 1){
		return true
	}
	let count = 0
	for (let i = 0; i < words1.length; i++) {
		const word = words1[i]
		if (words2.indexOf(word) != -1) {
			count++
			if (count >= threshold) {
				return true
			}
		}
	}
	return false
}

exports.minimumIntersection = (words1, words2, threshold) => {
	const w1 = new Set(words1)
	const intersection = w1.minimumIntersection(new Set(words2), threshold)
	if (intersection) {
		return  [...intersection]
	}
	return null
}

exports.tagPhraseFromCommonWords = (phrase, words) => {
	const wordSet = new Set(words)
	return phrase.split(SPLITTER_REGEX).filter(w => !!w).map(w => {
		return {
			word: w,
			match: wordSet.has(w.toUpperCase())
		}	
	})
} 
