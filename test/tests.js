const expect = require("chai").expect
const  NL = require("../src/index.js")

describe("Natural Language", () => {

	let phrase, words1, words2
	beforeEach(function (){
		phrase1 = "the rain in spain stays mainly on the plane"
		phrase2 = "the water in spain stays mostly on the plane"
		phrase3 = "the water,in spain,stays mostly     on the  ,, ,,plane"

		words1 = NL.importantWords(phrase1)
		words2 = NL.importantWords(phrase2)

		longPhrase1 = "the quick brown fox jumped over the lazy dog fire wind earth sun"
		longPhrase2 = "bambi noodle bladhow gordon noomy bongo lambda sun flip wind earth"
		longPhrase3 = "bambi noodle bladhow gordon noomy bongo lambda sun flip gloop earth"
	})

	it("should extract important words", function() {
		const words = NL.importantWords(phrase1)
		expect(words).to.have.lengthOf(5)
	})

	it("should extract important words including commas", function() {
		const words = NL.importantWords(phrase3)
		expect(words).to.have.lengthOf(5)
	})

	it("should extract important words and remove full stop", function() {
		const words = NL.importantWords("full stop.")
		expect(words).to.have.lengthOf(2)
		expect(words[1]).to.equal("STOP")
	})

	it("should return positive match", function() {
		expect(NL.phrasesMatch(words1,words2, 3)).to.be.true
	})

	it("should return negative match", function() {
		expect(NL.phrasesMatch(words1,words2, 4)).to.be.false
	})

	it("should return false if either words array undefined", function() {
		expect(NL.phrasesMatch(words1,undefined, 0)).to.be.false
	})

	it("mimimumIntersection should find array when more than positive match", function() {
		const result = NL.minimumIntersection(words1,words2, 2)
		expect(result).to.have.lengthOf(3)	
	})

	it("mimimumIntersection should find array when positive match", function() {
		const result = NL.minimumIntersection(words1,words2, 3)
		expect(result).to.have.lengthOf(3)	
	})

	it("mimimumIntersection should be null when negative match", function() {
		const result = NL.minimumIntersection(words1, words2, 4)
		expect(result).to.be.a('null');
	})

	it("mimimumIntersection should find array when 0 threshold", function() {
		const result = NL.minimumIntersection(words1, words2, 0)
		expect(result).to.have.lengthOf(3)	
	})

	it("mimimumIntersection should find empty array when 0 threshold and no matches", function() {
		const result = NL.minimumIntersection(["foo", "bar", "baz"], words2, 0)
		expect(result).to.have.lengthOf(0)	
		expect(result).to.be.instanceof(Array);
	})


	it("mimimumIntersection should find array when positive match long sentences", function() {
		let words1 = NL.importantWords(longPhrase1)
		words2 = NL.importantWords(longPhrase2)
		const result = NL.minimumIntersection(words1,words2, 3)
		expect(result).to.have.lengthOf(3)	
	})

	it("mimimumIntersection should be null array when neg match long sentences", function() {
		let words1 = NL.importantWords(longPhrase1)
		words2 = NL.importantWords(longPhrase3)
		const result = NL.minimumIntersection(words1,words2, 3)
		expect(result).to.be.a('null');
	})


	it("should generate tagPhraseFromWords", function() {
		const words = ["SPAIN", "STAYS", "PLANE"]
		const expectedResult = [
		{word: "the", 			match:false},
		{word: "water", 		match:false},
		{word: "in", 			match:false},
		{word: "spain", 		match:true},
		{word: "stays", 		match:true},
		{word: "mostly",  	match:false},
		{word: "on", 			match:false},
		{word: "the", 			match:false},
		{word: "plane", 		match:true}
		]
		expect(NL.tagPhraseFromCommonWords(phrase2,words)).to.eql(expectedResult)
	})

	it("should generate tagPhraseFromWords when no words given", function() {
		const words = []
		const expectedResult = [
		{word: "the", 			match:false},
		{word: "water", 		match:false},
		{word: "in", 			match:false},
		{word: "spain", 		match:true},
		{word: "stays", 		match:true},
		{word: "mostly",  	match:false},
		{word: "on", 			match:false},
		{word: "the", 			match:false},
		{word: "plane", 		match:true}
		]
		expect(NL.tagPhraseFromCommonWords(phrase2,words)).to.have.lengthOf(9)
	})

	it("should generate tag phrase From words when phrase has commas", function() {
		const words = ["SPAIN", "STAYS", "PLANE"]
		const expectedResult = [
		{word: "the", 			match:false},
		{word: "water", 		match:false},
		{word: "in", 			match:false},
		{word: "spain", 		match:true},
		{word: "stays", 		match:true},
		{word: "mostly",  	match:false},
		{word: "on", 			match:false},
		{word: "the", 			match:false},
		{word: "plane", 		match:true}
		]
		expect(NL.tagPhraseFromCommonWords(phrase3,words)).to.eql(expectedResult)
	})

	it("from actual app", function() {

		const myWords =  [
		"RAIN",
		"SPAIN",
		"MAINLY",
		"PLAIN"
		]

		const theirWords = [
		"RAIN",
		"FOO",
		"BAR",
		"BAZ"
		]

		const intersection =  NL.minimumIntersection(myWords, theirWords, 2)
		expect(intersection).to.be.a('null');
		
	})
})
