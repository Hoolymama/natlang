Natural Language Utilities
=========

Phrase similarities

## Installation

    npm install @hoolymama/natlang

## Usage

Get Important words from a phrase

    var NL = require("hoolymama/natlang")

    var phrase = "the rain in spain stays mainly on the plane"

    NL.importantWords(phrase)

    # result: ["RAIN", "SPAIN", "STAYS", "MAINLY", "PLANE"]

Determine if two arrays for words have some of the same words 


    var words1 = NL.importantWords( "the rain in spain stays mainly on the plane")
    var words2 = NL.importantWords( "the water in spain stays mostly on the plane")
    NL.phrasesMatch(words1,words2, 3)
    # result: true
    NL.phrasesMatch(words1,words2, 4)
    # result: false
   
## Tests

    npm test

## Contributing

You are sure to have better code style than me, so go for it.